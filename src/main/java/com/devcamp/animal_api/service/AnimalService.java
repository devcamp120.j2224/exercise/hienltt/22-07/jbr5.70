package com.devcamp.animal_api.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.animal_api.model.Animal;
import com.devcamp.animal_api.model.Cat;
import com.devcamp.animal_api.model.Dog;
@Service
public class AnimalService {
    Cat cat1 = new Cat("Cat1");
    Cat cat2 = new Cat("Cat2");

    Dog dog1 = new Dog("Dog 1");
    Dog dog2 = new Dog("Dog 2");

    public ArrayList<Animal> getAllAnimal(){
        ArrayList<Animal> lAnimals = new ArrayList<>();
        lAnimals.add(cat1);
        lAnimals.add(dog1);
        lAnimals.add(cat2);
        lAnimals.add(dog2);
        return lAnimals;
    }
}

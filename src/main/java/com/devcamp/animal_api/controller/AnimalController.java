package com.devcamp.animal_api.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.animal_api.model.*;
import com.devcamp.animal_api.service.AnimalService;

@RestController
@CrossOrigin
public class AnimalController {
    @Autowired
    AnimalService animalService;

    @GetMapping("/cats")
    public ArrayList<Cat> getListCat(){
        ArrayList<Cat> listCat = new ArrayList<>();

        ArrayList<Animal> listAllAnimal = animalService.getAllAnimal();
        for (Animal animal : listAllAnimal){
            if (animal instanceof Cat){
                listCat.add((Cat)animal);
            }
        }
        return listCat;
    }

    @GetMapping("/dogs")
    public ArrayList<Dog> getListDog(){
        ArrayList<Dog> listDog = new ArrayList<>();
        
        ArrayList<Animal> listAllAnimal = animalService.getAllAnimal();
        for (Animal animal : listAllAnimal){
            if (animal instanceof Dog){
                listDog.add((Dog)animal);
            }
        }

        return listDog;
    }
}
